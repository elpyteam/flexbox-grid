/*----------------------------------*\
    Scrollspy
\*----------------------------------*/
$(function() {
  /*
  	Scrollspy options with data attributes
  */
  /* Get all <a> items with href attribute that start with # */
  var scrollSpyItems = $('[data-scrollspy]').find('a[href^="#"]');
  /* Get Scrollspy navigation wrapper height to include it as offset */
  var scrollSpyWrapper = $('[data-scrollspy-wrapper]') ? $('[data-scrollspy-wrapper]').outerHeight() : 0;
  /* Get Scrollspy slide speed */
  var scrollSpySpeed = $('[data-scrollspy]').attr('data-scrollspy-speed') ? $('[data-scrollspy]').attr('data-scrollspy-speed') : 500;
  /* Get Scrollspy offset */
  var scrollSpyOffset = $('[data-scrollspy]').attr('data-scrollspy-offset') ? $('[data-scrollspy]').attr('data-scrollspy-offset') : 0;
  /*
  	Scrollspy scroll function
  */
  var scrollSpyScroll = function() {
    /* Get container scroll position */
    var scrollSpyPosition = $(this).scrollTop() + scrollSpyWrapper;
    $(scrollSpyItems).each(function() {
      /* Get Scrollspy item */
      var scrollSpyItem = $($(this).attr('href'));
      /* Add/Remove active class depending on scroll position */
      if (scrollSpyItem.offset().top <= scrollSpyPosition) {
        $(this).addClass('active').parent().siblings().children().removeClass('active');
      } else {
        $(this).removeClass('active');
      }
    });
  };
  /* Initiate Scrollspy scroll function on scroll */
  $(window).scroll(function() {
    scrollSpyScroll();
  });
  /*
  	Scrollspy function
  */
  var scrollSpy = function(e) {
    /* Prevent default action on <a> tag */
    e.preventDefault();
    /* Get Scrollspy <a> tag href attribute */
    var scrollSpyItemID = $(this).attr('href');
    /* Slide to content with animation */
    $('html, body').animate({
      scrollTop: $(scrollSpyItemID).offset().top - scrollSpyWrapper - scrollSpyOffset
    }, scrollSpySpeed);
  };
  /* Initiate Scrollspy function on click */
  $(scrollSpyItems).click(scrollSpy);
});
