/*----------------------------------*\
    Tabs
\*----------------------------------*/
$(function() {
  var tabs = $('[data-tabs]');
	tabs.find('a').click(function(e) {
		e.preventDefault();
		var tabID = $(this).attr('href');
		$(this).addClass('active').parent().siblings().children().removeClass('active');
		$(tabID).addClass('active').siblings().removeClass('active');
	});
});
